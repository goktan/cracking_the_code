
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
#include <tr1/unordered_map>
#include <typeinfo>

using namespace std;
using namespace tr1;

typedef unordered_multimap<std::string,std::string> stringmap;

string string_toupper(string str) {
    string tmp;
    for(char s : str) {
        tmp += toupper(s);
    }
    return tmp;
}

int main() {
    
    stringmap dict;
    dict.insert({"l", "looked"});
    dict.insert({"j", "just"});
    dict.insert({"l", "like"});
    dict.insert({"h", "her"});
    dict.insert({"b", "brother"});
    
    string str = "jessjustlookliketimherbrother";
    unsigned long int lastmatch = 0;
    for(unsigned long int i = 0; i<str.size(); ++i) {
        string target(str.substr(i,1));
        auto range = dict.equal_range(target);
        unsigned long maxlen=0;
        for_each (
                  range.first,
                  range.second,
                  [&](stringmap::value_type& x){
                      maxlen = max(maxlen, x.second.length());
                  }
                  );
        //cout <<target << "-> " <<  maxlen << endl;
        for(auto k=maxlen; k>1; k--) {
            string target2(str.substr(i, k));
            for(auto l = range.first; l != range.second; ++l) {
                if(l->second.compare(target2) == 0) {
                    //match
                    if(lastmatch!=i)
                        cout << string_toupper(str.substr(lastmatch, i-lastmatch)) << " " ;
                    cout << target2 << " ";
                    cout.flush();
                    i = i+k-1;
                    lastmatch = i+1;
                    goto endoffor;
                }
            }
        }
    endoffor:
        int unused = 0;
    }
    return 1;
}

