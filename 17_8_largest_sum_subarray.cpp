#include <cstdio>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;

int main(int argc, char *argv[]) {
#ifdef DEBUG
     freopen("input.txt", "r", stdin);
     ios :: sync_with_stdio(false);
     
#endif
    
    vector <int> a;
    a.clear();
    int max = 0, max_now = 0, max_in_array = INT_MIN;
    int m, y;
    cin >> m;
    while (m--) {
        cin >> y;
        a.push_back(y);
        
    }
    int j = 0, begin = 0, begin_tmp = 0, end = 0;
    for (const auto &i : a) {
        j++;
        if(i + max_now < 0) {
            begin_tmp = j+1;
            max_now = 0;
        } else {
            max_now = i + max_now;
        }
        if (max_now > max) {
            begin = begin_tmp;
            max = max_now;
            end = j;
        }
        max_in_array = std::max(max_in_array, i);
    }
    if(max==0) {
        cout << max_in_array << endl;
        cout << find(a.begin(), a.end(), max_in_array)-a.begin() << endl;
    }
    else {
        cout << max << endl;
        cout << begin << "  " << end << endl;
    }

    
    return 0;
}