#include <cstdio>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <vector>
#include <map>
#include <queue>
#include <string>
using namespace std;

typedef long long ll;

int main(int argc, char *argv[]) {
#ifdef DEBUG
     freopen("input.txt", "r", stdin);
     ios :: sync_with_stdio(false);
     
#endif
    fstream fin("input.txt", ios::in);
    string str, target1 = "goktan", target2 = "murat";
    map<string, queue<int> > stmap;
    int count = 0;
    while(fin.peek()!=EOF) {
        fin>>str;
        stmap[str].push(count);
        cout << str << " " << stmap[str].back() << " " << stmap[str].size()<<endl;
        count ++;
    }
    
    typedef struct {
        int pos;
        int belongs;
    }ST;
    
    vector<ST> stvec;
    queue<int> t1 (stmap[target1]);
    queue<int> t2 (stmap[target2]);
    
    while(!t1.empty() && !t2.empty()) {
        if(t1.front() < t2.front()) {
            ST s;
            s.pos = t1.front();
            s.belongs = 1;
            stvec.push_back(s);
            t1.pop();
        } else {
            ST s;
            s.pos = t2.front();
            s.belongs = 2;
            stvec.push_back(s);
            t2.pop();
        }
    }
    while(!t1.empty()) {
        ST s;
        s.pos = t1.front();
        s.belongs = 1;
        stvec.push_back(s);
        t1.pop();

    }
    while(!t2.empty()) {
        ST s;
        s.pos = t2.front();
        s.belongs = 2;
        stvec.push_back(s);
        t2.pop();
    }
    int minval = INT_MAX;
    for (int i = 1; i<stvec.size(); ++i) {
        if (stvec[i].pos - stvec[i-1].pos < minval) {
            if (stvec[i].belongs != stvec[i-1].belongs) {
                minval = stvec[i].pos - stvec[i-1].pos;
            }
        }
    }
    cout << minval << endl;
    //easy part
    /*
    int target1_pos = -1, target2_pos = -1;
    int count = 0;
    int min_len = INT_MAX;
    while(fin.peek()!=EOF) {
        fin>>str;
        if(str==target1) target1_pos = count;
        else if(str==target2) target2_pos = count;
        if(target1_pos!=-1 && target2_pos!=-1) {
            if(min_len > abs(target1_pos-target2_pos)) {
                min_len = abs(target1_pos-target2_pos);
            }
        }
        count++;
    }

    cout << (min_len!=INT_MAX?min_len:-1) << endl;
     */
    
    return 0;
}