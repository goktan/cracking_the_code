#include <iostream>
#include <vector>
#include <exception>
using namespace std;

template <class T>
class Stack {
	class Node {
	public:
		Node(T d) {data = d;}
		T data;
		Node * next;
    };
    int size;
    Node *head;
    
public:
	Stack() {
		head = NULL;
		size = 0;
	}
	void push(T d) {
		Node * n = new Node(d);
		n->next = head;
		head = n;
		size ++;
	}
	int getSize() {return size;}
	T pop() {
		if(!head) {
			cerr << "empty stack" << endl;
            throw out_of_range("empty stack");
		}
		size --;
		T n = head->data;
		head = head->next;
		return n;
	}
	T peek() {
		return head->data;
	}
};

template <class T>
class SetOfStacks {
	vector<Stack<T> > stack_vec;
	const int max_elements;
	int stack_num;

	bool addNewStack() {
		Stack<T> s;
		stack_num ++;
		stack_vec.push_back(s);
        cout<<"new stack added" << endl;
		return true;
	}
	bool removeStack() {
		if(stack_num>0) {
            stack_num --;
            stack_vec.pop_back();
            cout<<"stack removed" << endl;
            return true;
        }
        return false;
	}
	bool verifyStackAmount() {
		if(stack_vec[stack_num].getSize() == 0) {
			return removeStack();
		}
		return true;
	}
public:
    SetOfStacks() : max_elements(5) {
		stack_num = -1;
		addNewStack();
	}
	void push(T d) {
		if(stack_vec[stack_num].getSize() == max_elements) {
			addNewStack();
		}
		stack_vec[stack_num].push(d);
	}
	T pop() {
        verifyStackAmount();
		return stack_vec[stack_num].pop();	
	}
};

int main() {
    SetOfStacks<int> s;
    s.push(5);
    s.push(4);
    s.push(4);
    s.push(4);
    s.push(4);
    s.push(4);
    s.push(4);
    s.push(4);
    s.push(4);
    s.push(4);
    s.push(4);
    s.push(4);
    s.pop();
    s.pop();
    s.pop();
    s.pop();
    s.pop();
    s.pop();
    s.pop();
    s.pop();
    s.pop();
    s.pop();
    s.pop();
}



