
//beware the code is not checking if the sides are all one, instead it considers the beginning part of square is 1. just for simplicity
#include <iostream>
using namespace std;
const int SQUARE_SIZE=10;
int big_square[SQUARE_SIZE][SQUARE_SIZE] = {1,1,1,1,1,1,1,1,1,1,
                                            1,0,0,0,0,1,1,1,1,1,
                                            1,0,0,0,0,1,1,1,1,1,
                                            1,0,0,0,0,1,1,0,1,1,
                                            1,0,0,0,0,1,1,1,1,1,
                                            1,1,1,1,1,1,1,1,1,1,
                                            1,1,1,1,0,0,0,0,0,1,
                                            1,1,1,1,1,1,0,0,0,1,
                                            1,1,1,1,1,1,1,1,1,1,
                                            1,1,1,1,1,1,1,1,1,1};

bool detect_square(int x, int y, int &size) {
    int to_right = 0;
    int line_size = 0;
    int old_to_right = 0;
    int to_down = SQUARE_SIZE;

    for (int j=x; j<SQUARE_SIZE; ++j) {
        if (big_square[y][j] == 0) {
            big_square[y][j] = 2;
            to_right ++;
        } else {
            old_to_right = to_right;
            to_down = to_right+y;
            if (to_down >= SQUARE_SIZE) { //we exceed max down size, no chance to be a square
                cout << "x size for possible square is " << to_right << " but there is no room for it" << endl;
                size = -1;
                return false;
            }
            line_size = to_right + x;
            break;
        }
    }
    cout << "x size for possible square is " << to_right << endl;
    cout << "I'll check more down to " << to_down << endl;
    for (int i=y+1; i<to_down; ++i) {
        to_right = 0;
        for (int j=x; j<line_size; ++j) {
            if (big_square[i][j] == 0) {
                big_square[i][j] = 2;
                to_right ++;
            } else {
                if(old_to_right != to_right) {
                    cout << "I got " << to_right << " instead of " << old_to_right << endl;
                    size = -1;
                    return false;
                }
            }
        }
        cout << "line " << i << "also has " << to_right << "zeros " << endl;
    }
    size = to_right;
    return true;
}

int main() {
    int size = 0, min_size = INT_MAX;
    for (int i = 0; i<SQUARE_SIZE ;++i) {
        for (int j = 0; j<SQUARE_SIZE; ++j) {
            if (big_square[i][j] == 0) {//if white
                if(detect_square(j, i, size)) {
                    min_size = min(size,min_size);
                }
            }
        }
    }
    cout << min_size << endl;
    
    return EXIT_SUCCESS;
}