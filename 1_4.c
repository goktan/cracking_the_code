//
//  main.c
//  cracking_the_code_c
//
//  Created by goktan kantarcioglu on 13/04/14.
//  Copyright (c) 2014 goktan kantarcioglu. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void replace_spaces(char *arr) {
    int len = 0;
    int i, j;
    while(arr[len]!='\0') {
        ++len;
    }
    len = len - 1;
    if(len < 0) return;
    
    for(i = len; i>=0; --i) {
        if(arr[i] == ' ') {
            for(j = len+1; j>i; --j) {
                arr[j+2] = arr[j];
            }
            len = len + 2;
            arr[i] = '%';
            arr[i+1] = '2';
            arr[i+2] = '0';
        }
    }
    return;
}

int main(int argc, const char * argv[])
{
    char * str = (char *)malloc(sizeof(char) * 50);
    strncpy(str, "gok tan1 tan 11\0", 16);
    printf("%s\n", str);
    replace_spaces(str);
    printf("%s\n", str);
    free(str);
    return 0;
}
