//this is the modified version of question, see below for details.
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
using namespace std;

template<typename T, size_t N>
T * array_end(T (&ra)[N]) {
    return ra + N;
}

template<typename T, size_t N> constexpr size_t array_len(T (&)[N]) {return N;}

bool comp_by_size(const string &str1, const string &str2) {
    return str1.size() > str2.size();
}

void print_vector(vector<string> strvec) {
    for(const auto &str: strvec)
        cout << str << " " ;
    cout << endl;
}

bool is_consist_word(const string & checkstr, bool is_original, map<string, bool> & dict_map) {
    if(dict_map.find(checkstr) != dict_map.end() && !is_original) {
        return dict_map.at(checkstr);
    }
    for(int i=1; i<checkstr.length(); ++i) {
        auto left = checkstr.substr(0, i);
        auto right = checkstr.substr(i);
        bool wastrue = false;
        //in original question, it was detecting as true if we find a word as the concetanation of any words in dict list
        //in this case "goktangoktangoktan" was considered as true. but I changed the question to concetanation of different dict members
        //so I invented wastrue flag, if I find left is there already, I temporarily set its value to false so I don't find it in remaining checks of the same
        //word. and finally I set the original value to the dict key.
        if(dict_map.find(left) != dict_map.end() && dict_map.at(left) == true && (wastrue=dict_map[left], dict_map[left] = false, is_consist_word(right, false, dict_map))) {
            if(wastrue) dict_map[left] = wastrue, wastrue=false;
            return true;
        }
        if(wastrue) dict_map[left] = wastrue, wastrue=false;
    }
    dict_map[checkstr] = false;
    return false;
}

int main() {
    const char *dict_init[] = {"goktan", "murat", "kantarcioglu", "good", "days"};
    const char *wordlist_init[] = {"goktangoktangoktan", "muratgood", "kantarciogludays", "goktanmuratkantarciogl", "days"};
    vector<string> dict(dict_init, array_end(dict_init));
    vector<string> wordlist(wordlist_init, array_end(wordlist_init));
    map<string, bool> dict_map;
    sort(wordlist.begin(), wordlist.end(), comp_by_size);
    
    for(const auto& str: dict) {
        dict_map[str] = true;
    }
    
    print_vector(wordlist);
    
    for(const auto &str: wordlist) {
        if(is_consist_word(str, true, dict_map)) {
            cout << str << endl;
            break;
        }
    }
    
    return 1;
}

