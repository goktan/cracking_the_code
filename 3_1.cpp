#include <iostream>
using namespace std;

const int SIZE = 10;

void push(int *stack, int * sp, int data) {
	int *end = stack + (sizeof(int)*SIZE);
	if(end == sp) {
		cerr << "stack is full, try another one" << endl;
	}
	*sp =data;
	sp --;
}

int pop(int *stack, int *sp) {
	int *end = stack + (sizeof(int)*SIZE);
	if(sp == end) {
		cerr << "stack is empty" << endl;
		return -1;
	}
    int data = *sp;
    sp ++;
    return data;
}

int main(void) {

    int array[SIZE * 3];
    memset(array, 0, SIZE);
    int *s1, *s2, *s3;
    int *sp1, *sp2, *sp3;
    s1 = &array[0];
    s2 = &array[SIZE];
    s3 = &array[SIZE*2];
    
    sp1 = &array[SIZE-1];
    sp2 = &array[SIZE*2-1];
    sp3 = &array[SIZE*3-1];
    
    push(s1, sp1, 3);
    push(s1, sp1, 3);
    push(s1, sp1, 3);
    int c = pop(s1, sp1);
    cout << c << endl;
    
    push(s2, sp2, 3);
    push(s2, sp2, 3);
    push(s2, sp2, 3);
    int d = pop(s2, sp2);
    cout << d << endl;
    
    return 0;
}



/* second solution

#include <iostream>
#include <exception>

using namespace std;

const int SIZE = 100;
int* buf = new int [SIZE*3];
int* sp = new int [3];


bool isFull(int stackNum) {
    if(sp[stackNum] >= (SIZE - 1) )
        return true;
    return false;
}
bool isEmpty(int stackNum) {
    if(sp[stackNum] == -1 )
        return true;
    return false;
}

int originalPosition(int stackNum) {
    return (stackNum*SIZE)+sp[stackNum];
}
bool push(int stackNum, int val) {
    if(isFull(stackNum)) {
        cerr << "stack is full " <<  endl;
        return false;
    }
    sp[stackNum]++;
    int realPos = originalPosition(stackNum);
    buf[realPos] = val;
    return true;
}

int pop(int stackNum) {
    if(isEmpty(stackNum)) {
        cerr << "stack is empty" << endl;
        return -1;
    }
    int realPos = originalPosition(stackNum);
    sp[stackNum] --;
    return buf[realPos];
}
                            
                            
                            
                            
int main(void) {
    sp[0] = sp[1] = sp[2] = -1;
    push(1, 3);
    push(1, 5);
    push(2, 6);
    push(1, 8);
    cout << pop(1) << endl;
                                
    return 0;
}

*/