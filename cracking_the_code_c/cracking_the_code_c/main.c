//
//  main.c
//  cracking_the_code_c
//
//  Created by goktan kantarcioglu on 13/04/14.
//  Copyright (c) 2014 goktan kantarcioglu. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>



char * compress_str(char *str) {
    int i = 0;
    unsigned long int len = 0;
    char prev = '\0';
    int totalchar = 1;
    int newtotal = 0;
    int addon = 0;
    char * compstr, *compstr_ptr;
    len = strlen(str);
    compstr = (char *)calloc(sizeof(char), len);
    compstr_ptr = compstr;
    while(1) {
        if(i>=len) {
            addon = log10(totalchar) + 1 + 1; // 1 missing from log one for the compressed char
            
            if (newtotal+addon > len) {
                return strdup(str);
            }
            snprintf( compstr_ptr, addon+1, "%c%d", prev, totalchar);
            newtotal += addon;
            prev = str[i];
            totalchar = 1;

            break;
        }
        if(prev == '\0') {
            prev = str[i];
            i ++;
            continue;
        }
        if(prev == str[i]) {
            totalchar ++;
        } else {
            addon = log10(totalchar) + 1 + 1; // 1 missing from log one for the compressed char
            
            if (newtotal+addon > len) {
                return strdup(str);
            }
            addon = snprintf( compstr_ptr, addon+1, "%c%d", prev, totalchar);
            printf("%c%d", prev, totalchar);
            fflush(stdout);
            if(addon < 0) {
                printf("Error on snprintf\n");
                return strdup(str);
            }
            compstr_ptr += addon;
            newtotal += addon;
            prev = str[i];
            totalchar = 1;
        }
        i++;
    }
    
    return compstr;
}

int main(int argc, const char * argv[])
{
    char * str = (char *)calloc(sizeof(char), 50);
    char * comp;
    strcpy(str, "aaaaqqppcaaaaaaaq");
    comp = compress_str(str);
    printf("%s\n", str);
    printf("%s\n", comp);
    for (int i = 0;i<50;++i) {
        printf("%c",comp[i]);
    }
    free(comp);
    free(str);
    return 0;
}
