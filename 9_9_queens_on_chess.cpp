//
//  main.cpp
//  queens_on_chess
//
//  Created by goktan kantarcioglu on 11/04/14.
//  Everything by purpose written in C form(except printing on screen). Idea is to manage memory and get away from easiness of C++
//

#include <iostream>
using namespace std;

typedef short BOARDTYPE;
const int SIZE = 8;
void copy_board(BOARDTYPE new_board[SIZE][SIZE], BOARDTYPE old_board[SIZE][SIZE]);

typedef struct Coordinate {
    int x;
    int y;
} Coordinate;

typedef struct State {
    Coordinate c;
    BOARDTYPE br[SIZE][SIZE];
}State;

typedef struct Node {
    Node * next;
    State state;
} Node;

typedef struct Stack {
    Node * head;
    Stack () {
        head = NULL;
    }
    void push_back(State st) {
        Node *n = (Node*)malloc(sizeof(Node));
        memset((void*)n, 0, sizeof(Node));
        
        n->state.c = st.c;
        copy_board(n->state.br, st.br);
        n->next = this->head;
        this->head = n;
    }
    
    void pop_back(State *st) {
        Node * top_val = head;
        if(!head) {
            cerr << "Stack is already empty!" << endl;
            st = NULL;
            return;
        }
        st->c = top_val->state.c;
        copy_board(st->br,top_val->state.br);
        head = head->next;
        free(top_val);
        return;
    }
    
    State* back(void) {
        return this->head==NULL?NULL:&(this->head->state);
    }
} Stack;


Stack stack;

BOARDTYPE board [SIZE][SIZE] ={0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0};

void copy_board(BOARDTYPE new_board[SIZE][SIZE], BOARDTYPE old_board[SIZE][SIZE] = board) {
    for(int i=0; i<SIZE; ++i){
        for(int j=0; j<SIZE; ++j ) {
            (new_board)[i][j] = old_board[i][j];
        }
        
    }
}

void print_board(BOARDTYPE bp[SIZE][SIZE] = board) {
    char ch = '?';
    for(int i=0; i<SIZE; ++i){
        for(int j=0; j<SIZE; ++j ) {
            if(bp[i][j]==0) ch = '.';
            if(bp[i][j]==1) ch = 'x';
            if(bp[i][j]==2) ch = 'o';
            if(bp[i][j]==3) ch = '|';
            cout<<ch<<" ";
        }
        cout << endl;
    }
    cout << endl;
}

void queen_attack(Coordinate c) {
    //fill row
    for(int i=0; i<SIZE; i++) {
        if(board[c.x][i] == 0)
            board[c.x][i] = 2;
    }
    //fill column
    for(int j=0; j< SIZE; j++) {
        if(board[j][c.y]==0)
            board[j][c.y] = 2;
    }
    //fill cross
    int i = c.x+1;
    int j = c.y+1;
    int k = c.x-1;
    int l = c.y-1;
    while(i != SIZE || j!=SIZE || k != -1 || l != -1) {
        
        if(i<SIZE && j<SIZE)
            if(board[i][j]==0) {
                board[i][j] = 2;
            }
        if(i<SIZE && l>-1)
            if(board[i][l]==0) {
                board[i][l] = 2;
            }
        if(k>-1 && j<SIZE)
            if(board[k][j]==0 ) {
                board[k][j] = 2;
            }
        if(k>-1 && l>-1)
            if(board[k][l]==0) {
                board[k][l] = 2;
            }
        i<SIZE?i++:i; j<SIZE?j++:j; k>=0?k--:k; l>=0?l--:l;
    }

    
}

Coordinate check_for_the_next_best(BOARDTYPE br[SIZE][SIZE] = board) {
    Coordinate c;
    for(int i=0; i<SIZE; ++i){
        for(int j=0; j<SIZE; ++j) {
            if(br[i][j]==0) {
                c.x=i;
                c.y=j;
                return c;
            }
        }
    }
    c.x = SIZE;
    c.y = SIZE;
    return c;
}

void add_new_queen(Coordinate c, int *nth) {
    State st;
    board[c.x][c.y] = 1;
    queen_attack(c);
    st.c = c;
    copy_board(st.br);
    (*nth)++;
    stack.push_back(st);
}

int rollback_queen_and_mark(int back_count = 1) {
    Coordinate next_best_c;
    State *prev_state;
    State tmp;
//    cout << "Rollback in operation" << endl;
    stack.pop_back(&tmp); //remove last filled one
    
    prev_state = stack.back(); //get the possible previous check point
    if(!prev_state) {
        return back_count; //already back to beginning of search, stack is empty
    }
    next_best_c = check_for_the_next_best(prev_state->br); //find previous boards next best and mark it to not put again a queen
    prev_state->br[next_best_c.x][next_best_c.y] = 3;
    
    next_best_c = check_for_the_next_best(prev_state->br);//now check the next best and see if there is another slot empty
    if(next_best_c.x == SIZE && next_best_c.y == SIZE) {
        //we are already at the end of this option, move to next.
//        cout << "Remove one more" << endl;
        back_count = rollback_queen_and_mark(back_count+1); //remove one more and test again
    } else {
    //Restoring the altered board back into stack
        copy_board(board, prev_state->br);
    }
    //done here go back to original algorithm
    return back_count;
}

void print_stack_board(Node *nd) {
    if(nd != NULL) {
        print_board(nd->state.br);
        print_stack_board(nd->next);
    }
    
}

int main(int argc, const char * argv[])
{
    int hit = 0;
    Coordinate c;
    c.x = 0;
    c.y = 0;
    int nth = 0;
    State initial;
    copy_board(initial.br, board);
    initial.c = c;
    stack.push_back(initial);

    std::cout << "Hello, Chess!\n";
//    print_board();
    
    for(;c.x<SIZE;) {
        for(;c.y<SIZE;){
//            cout<<"Add to " << c.x << " - " << c.y << endl;
            add_new_queen(c, &nth);
//            cout << "Queens -> " << nth << endl;
            if(nth == 8) {
                hit++;
                cout << "Now there should be 8 Queens for " << hit << " time(s) !!!" << endl;
                print_board();

            }
//            print_board();
            c = check_for_the_next_best();
//            cout << "Next best " << c.x << " " << c.y << endl;
            if(c.x==SIZE && c.y==SIZE) {
                int back_count = rollback_queen_and_mark();
//                cout << "back count -> " << back_count << endl;
                nth = nth - back_count;
//                print_board();
                c = check_for_the_next_best();
            }
        }
    }
    
    return 1;
}

