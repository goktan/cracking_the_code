//
//  main.c
//  cracking_the_code_c
//
//  Created by goktan kantarcioglu on 13/04/14.
//  Copyright (c) 2014 goktan kantarcioglu. All rights reserved.
//

#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
using namespace std;

class Node {
private:
    int data;
    Node (int d) {
        data = d;
        next = NULL;
    }
public:
    Node * next;
    Node * addNewtoBegin(int d ) {
        Node* newnode = new Node(d);
        newnode->next = this;
        return newnode;
    }
    void addNewtoEnd(int d) {
        Node *tail = this;
        while(tail->next != NULL) {
            tail = tail->next;
        }
        tail->next = new Node(d);
        tail->next->next = NULL;
    }
    int getData() {
        return data;
    }
    static Node * initial(int d) {
        return new Node(d);
    }
    
};

Node* returnKth(Node *n, int k){
    if(!n) return NULL;
    Node *iter1 = n;
    Node *iter2 = n;
    for(int i = 0; i<k;++i) {
        if(!iter2) return NULL;
        iter2=iter2->next;
    }
    if(!iter2) return NULL;
    while(iter2) {
        iter2=iter2->next;
        iter1=iter1->next;
    }
    return iter1;
}

Node* returnKth_recursive(Node *n, int k, int &size) {
    if(!n) return NULL;
    Node * iter;
    iter =  returnKth_recursive(n->next, k, size);
    size ++;
    if(size==k) {
        return n;
    }
    
    return iter;
}

int main(int argc, const char * argv[])
{
    Node * n = Node::initial(3);
    n->addNewtoEnd(7);
    n->addNewtoEnd(17);
    n->addNewtoEnd(74);
    n->addNewtoEnd(72);
    n->addNewtoEnd(12);
    n->addNewtoEnd(7);
    n->addNewtoEnd(1);
    n->addNewtoEnd(5);
    
    Node * iter = n;
    while(iter != NULL ) {
        cout << iter->getData() << endl;
        iter = iter->next;

    }
    cout << endl;
    int size = 0;
    cout << returnKth(n, 3)->getData() << endl;
    cout << returnKth_recursive(n, 3, size)->getData() << endl;

    
    return 0;
}
