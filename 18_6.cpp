#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
using namespace std;

const int MAXELEMENT = 100;
const int FIRSTN = 25;
const int ARRAY_SIZE = 100;

void fill_random_array(int array[], int size) {
    for (int i=0; i < size; ++i) {
        array[i] = rand()%MAXELEMENT;
    }
}
void print_array(int array[], int size) {
    for (int i=0; i < size; ++i) {
        cout << array[i] << " ";
    }
    cout << endl;
}

int partition_array(int array[], int left, int right) {
    int pivot = array[(right+left)/2];
    while (left < right) {
        while (array[left] < pivot ) left ++;
        while (array[right] > pivot )right --;
        if(array[left] == array[right]) return left;
        if (left < right) {
            int tmp = array[left];
            array[left] = array[right];
            array[right] = tmp;
            left ++;
            right --;
        }
    }
    return left ;
}

int main() {
    srand((unsigned int ) time(NULL));
    int array[ARRAY_SIZE];
    memset(array, 0, sizeof(int)*ARRAY_SIZE);
    fill_random_array(array, ARRAY_SIZE);
    int partition_left = 0;
    int partition_num = ARRAY_SIZE-1;
    int partition_right = ARRAY_SIZE-1;
    while (true) {
        partition_num = partition_array(array, partition_left, partition_right);
        if(partition_num < FIRSTN) {
            partition_left = partition_num + 1;
        } else if (partition_num > FIRSTN ){
            partition_right = partition_num;
        } else break;
    }
    print_array(array, ARRAY_SIZE);
    cout << array[partition_num] << endl;
    return 1;
}

