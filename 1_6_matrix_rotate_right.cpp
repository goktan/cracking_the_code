#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
#include <type_traits>
using namespace std;

template <class T, size_t K>
void print_arr(T (&arr)[K]) {
    for(int i=0; i<K; ++i) {
        for(int j=0; j<K; ++j) {
            cout.width(2);
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }
}

template <class T, size_t K>
void swap_edges(T (&arr)[K], int edge) {
    int size = K-1-edge;
    typedef typename std::remove_reference<decltype(arr[0][0])>::type ArrType;
    for(int z=0, i=edge; i<size;++i, ++z) {

        ArrType tmp = arr[i][size];
        arr[i][size] = arr[edge][i];
        
        ArrType tmp1 = arr[size][size-z];
        arr[size][size-z] = tmp;
        
        tmp = arr[size-z][edge];
        arr[size-z][edge] = tmp1;

        arr[edge][i] = tmp;

    }
}

template <class T, size_t K>
void swap_edges1(T (&arr)[K], int edge) {
    int size = K-1-edge;
    typedef typename std::remove_reference<decltype(arr[0][0])>::type ArrType;
    for(int z=0, i=edge; i<size; ++i, ++z) {
        swap(arr[edge][i], arr[i][size]);
        swap(arr[edge][i], arr[size][size-z]);
        swap(arr[edge][i], arr[size-z][edge]);
    }
}




int main() {
    int arr[5][5] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24};
    //int arr[3][3] = {0,1,2,3,4,5,6,7,8};
    int size = sizeof(arr) / sizeof(arr[0]);
    print_arr(arr);
    for (int i=0; i<size/2; ++i) {
        swap_edges1(arr, i);
    }
    cout << endl << endl;
    print_arr(arr);
    return 0;
}

