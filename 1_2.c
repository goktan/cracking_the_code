//
//  main.c
//  cracking_the_code_c
//
//  Created by goktan kantarcioglu on 13/04/14.
//  Copyright (c) 2014 goktan kantarcioglu. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void reverse(char *arr) {
    int i=0;
    int k, j;
    while(arr[i]!='\0') {
        ++i;
    }
    k = i - 1;
    for(j=0; j<i/2; ++j) {
        int tmp = arr[j];
        arr[j]=arr[k-j];
        arr[k-j]=tmp;
    }
}

int main(int argc, const char * argv[])
{
    char * str = (char *)malloc(sizeof(char) * 7);
    strncpy(str, "goktan\0", 7);
    reverse(str);
    printf("%s\n", str);
    free(str);
    return 0;
}
