#include <string>
#include <iostream>
#include <sstream>
#include <cmath>
#include <stack>
using namespace std;

string names[10] = {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
string tens[10] = {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
string below_hundreds[10] = {"", "ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"};
string addons[10] = {"", "thousand", "million", "billion"};

string giveEnglish(int val) {
    
    stack <string> st;
    stringstream stst;
    int tmp = val;
    int tencount = 0;
    int prev = 0;
    int hundredcount = 0;
    bool hundredflag = false;
    if (tmp < 0) {
        tmp = abs(tmp);
    }
    while (tmp > 0) {
        
        int num = (tmp % 10);
        tmp = tmp / 10;
        if (hundredflag) {
            st.push(addons[hundredcount]);
            hundredflag = false;
        }
        
        if(tencount == 2) {
            st.push("hundred");
        }
        if (tencount == 1 && num == 1) {
            st.pop();
            st.pop();
            st.push(tens[prev]);
        } else if (tencount == 1) {
            st.push(below_hundreds[num]);
        }else {
            st.push(names[num]);
        }
        st.push(" ");
        prev = num;
        tencount ++;
        if (tencount == 3) {
            hundredcount ++;
            hundredflag = true;
            tencount = 0;
        }
        
    }
    if (val < 0) {
        st.push("minus ");
    }
    while(!st.empty()) {
        stst << st.top();
        st.pop();
    }
    
    return stst.str();
}


int main(int argc, const char * argv[])
{
    cout << "euler program" << endl;
    cout << giveEnglish(-12) << endl;
    return 0;
}

