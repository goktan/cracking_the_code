#include <cstdio>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;

void shuffle_deck(int deck[52]) {
    int rn = 0;
    for (int i=1; i<=52; ++i) {
        rn = rand()%52;
        while (deck[rn]!=0) {
            rn = rand()%52;
        }
        if(i == 0) {
            cout <<""<<endl;
        }
        deck[rn] = i;
    }
}

int main(int argc, char *argv[]) {
#ifdef DEBUG
     freopen("input.txt", "r", stdin);
     ios :: sync_with_stdio(false);
     
#endif
    srand((unsigned int)time(NULL));
    int deck[52];
    memset(deck, 0, sizeof(int)*52);
    shuffle_deck(deck);
    for(int i:deck) cout << i << " ";
    cout << endl;
    vector<int> v (deck, deck + sizeof deck / sizeof deck[0]);
    sort(v.begin(), v.end());
    for(int i:v) cout << i << " ";
    cout << endl;

    return 0;
}