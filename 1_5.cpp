//
//  main.c
//  cracking_the_code_c
//
//  Created by goktan kantarcioglu on 13/04/14.
//  Copyright (c) 2014 goktan kantarcioglu. All rights reserved.
//

#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
using namespace std;

unsigned long compressed_size(string);

string compress_str(string str) {
    stringstream comp_stream;
    unsigned long len = str.length();
    if(len == 0) return str;
    if(compressed_size(str) > str.length()) {
        return str;
    }
    int count = 1;
    char last = str[0];
    for (int i = 1; i < len; ++i) {
        if(str[i] == last) {
            count++;
        }
        else {
            comp_stream << last << count;
            count = 1;
            last = str[i];
        }
    }
    comp_stream << last << count;
    return comp_stream.str();

}

unsigned long compressed_size(string str) {
    int count = 1;
    char last = str[0];
    int total = 0;
    unsigned long len = str.length();
    for (int i = 1; i < len; ++i) {
        if(str[i] == last) {
            count++;
        }
        else {
            total += (int) log10(count) + 1 + 1;
            count = 1;
            last = str[i];
        }
    }
    return total;
}

int main(int argc, const char * argv[])
{
    string comp = compress_str("gggggaa");
    cout << comp << endl;
    return 0;
}
